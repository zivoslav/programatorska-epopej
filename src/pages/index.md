# Programátorská epopej

<?php const __BOOK__ = __DIR__ . '/../book'; ?>

<?php require __BOOK__ . '/oč-jde.md'; ?>
<?php require __BOOK__ . '/proč-epopej.md'; ?>

<?php require __BOOK__ . '/správa-verzí.md'; ?>
<?php require __BOOK__ . '/operační-systém.md'; ?>
<?php require __BOOK__ . '/git.md'; ?>
<?php require __BOOK__ . '/volba-editoru.md'; ?>
<?php require __BOOK__ . '/hello-world-v-mnoha-jazycích.md'; ?>
