## první program

Konečně jdeme programovat. Je tu ještě otázka volby programovacího jazyka. To je opravdu těžké volba. Počkáme s ní na později a ukážeme si první program v několika různých jazycích.

> Bývá zvykem začít jednoduchým programem, který vypíše pozdrav "Hello world". Právě tímto programem začíná kniha *The C Programming Language*, kterou napsali autoři tohoto programovacího jazyka Brian Kernighan a Dennis Ritchie. Stalo se tak už v roce 1978 (to mě byly tři roky) a od té doby se to tak používá.

### Nim

Nejjednodušší program typu *Hello world* by mohl vypadat takto.

<?php const __HELLO_WORLD__ = __dir__ . "/../examples/hello-world"; ?>

```nim
# src/main.nim
<?php require __HELLO_WORLD__ . "/src/main.nim"; ?>
```

Takto se to píše v mém oblíbeném jazyce [Nim](https://nim-lang.org/). Tento jazyk bohužel nepatří k těm příliš používaným. Nicméně se na něm velice dobře vysvětlují principy mprogramování. On si totiž tento jazyk vzal to nejlepší z jiných jazyků. No, posuď sám. A sám si vyber svůj jazyk.

*Nim* je jazyk kompilovaný, to znamená, že potřebuješ překladač. Překladač vezme tvůj zdrojový kód a vytvoří z něj spustitelný soubor.

```bash

nim compile --run src/main.nim 

```

Tento příkaz program přeložil a rovnou aj spustil. Ke stejnému výsledku bys došel aj takto:

```bash

nim compile src/main.nim
./src/main

```

### Python

Zkusme jiný jazyk. Mezi nejpoužívanější a nejoblíbenější jazyky patří jazyk *Python*.

```python
# src/main.py
<?php require __HELLO_WORLD__ . "/src/main.py"; ?>
```

Zdrojový kód je podobně jednoduchý, jako ten v jazyce *Nim*. Pravda je taková, že *Nim*, který je poměrně novým jazykem, od jazyka *Python* opravdu hodně opisoval. I když zrovna v tomto jednoduchém příkladě se oba jazyky liší už použitým příkazem - `echo` místo `print`. Závorky kolem "hello world" můžeme napsat i v Nimu a ve starších verzích Pythonu se naopak nepsaly - v tom až takový rozdíl není.

Zásadní rozdíl mezi oběma jazyky spočívá v něčem jiném. 

*Python* není jazyk kompilovaný, ale jazyk interpretovaný. To znamená, že se nepřeloží do spustitelného souboru, ale program zvaný interpret jazyka čte zdrojový kód řádek po řádku. Každý řádek (ve skutečnosti každý příkaz - na jednom řádku může být víc příkazů a nebo taky může být jeden příkaz na více řádcích) vyhodnotí a spustí.

Interpret a kompilátor dělají vlastně podobnou práci. Interpet však zdrojový kód zpracovává při každém spuštění zas a znova. Kompilátor pouze při kompilaci.

Když bude ve zdrojovém kódu chyba, interpretovaný program selže při spuštění. Kompilovaný už při kompilaci. Takže když se program zkompiluje bez chyby, půjde bez chyby i spouštět. U interpretovaného programu se může chyba objevit až po několikátém spuštění, nemám jistotu, že program poběží bez chyby (chyby mohou být v podmínkách, či cyklech, které se vyhodnotí jen někdy, za určitých podmínek).

Kompilátor jazyka *Nim" se jmenuje *nim* a interpet jazyka "Python" se jmenuje "python". 

> Možná se ten interpret na tvém počítači jmenuje "python3", protože tento jazyk měl trochu delší anabázi se svou modernizací a přechod z verze 2 na verzi 3 trval dlouhé roky. Na mnoha počítačích může být zároveň python 2.x a python 3.x. Příkaz "python" spustí starší verzi jazyka, ve které ale zrovna tahle verze "Hello world" nebude fungovat.


```bash
python src/main.py

```

### PHP

Webové stránky se velice často vytváří v jazyce PHP. A v PHP napíšeš stejný program takto.

```php
# src/main.php
<?php require __HELLO_WORLD__ . "/src/main.php"; ?>
```

Je to o něco složitější. Jednak přibyl středník na konci příkazu. To je syntaxe, kterou *PHP* převzalo z jazyka *C*. Těch inspirací z jazyka *C* je mnohem více, tento jazyk totiž udal určitý standart a mnohé novější jazyky se z něj více, nebo méně inspirují.

Na začátek jsme museli napsat podivnou směs znaků `<?php`. Je to zvláštnost jazyka *PHP*. Tento jazyk byl totiž primárně vymyšlen k tomu, aby se jeho zdrojový kód míchal s obyčejným textem, nebo spíše s jazykem *html*. *PHP* je vlastně šablonovací jazyk. I když jeho pozdější vývoj jde jiným směrem a za původní ideály, kvůli kterým vznikl se poněkud stydí. S tím tak úplně nesouhlasí původní autor jazyka *PHP* Rasmus Lerdorf a říká, že moderní *PHP* jde poněkud [špatnou cestou](https://phpthewrongway.com/).

Zdrojový kód *PHP* ukončíme značkou `?>`, i když to není vždy potřeba a ani se to nedoporučuje, pokud máme v souboru jen a pouze *PHP* kód. Pokud mícháme *PHP* s textem (zpravidla používáme značkovací jazyk *html*, což je také vlstně čistý text ), pak *PHP* kód musíme řádně ukončit.

```php
# src/main.php
<?php require __HELLO_WORLD__ . "/src/main.phtml"; ?>
```

Program v *PHP* spustíme takto.

```bash

php src/main.php;

```

A vypsalo se totéž co v předchozím programu v jazyce *Nim*.

Druhý zdrojový soubor spustíme stejně.

```bash

php src/main.phtml;

```

Vypsalo se i vše, co není součástí kódu *PHP*. Nemusíme nutně *PHP* vkládat jen do *html*, i když to je jeho primární smysl použití. Ono totiž je to takto. *Python* je jazyk, který se hodně používá třeba i v tom terminálu na různé skripty, ale i na grafické programy. V *Pythonu* jsou naprogramovány různé nástroje pro správu operačního systému, nastavení počítače, hromadné zpracování dat, analýza DNA, na co si vzpomeneš. *PHP* bylo vymyšleno účelově na něco jiného. Jeho základní použití je na webovém serveru. *PHP* bylo vymyšleno jako snandá cesta, jak dynamicky generovat *html* stránky. Proto ta kombinace s *html*. V principu ale není problém používat *PHP* jako univerzální šablonovací systém. To dělám při psaní programátorské epopeje. 

### jazyk C

Říkal jsem, že jazyk *C* je takový standard. Ten úplně první *hello world* z knihy Briana Kernighana a Dennise Ritchiho vypadá takto. 

```c
# src/main.c
<?php require __HELLO_WORLD__ . "/src/main.c"; ?>
```

To už je trochu víc psaní. Pomocí directivy `#include <stdio.h>` vložíme do našeho zdrojového kódu ještě jiný zdrojový kód, který už za nás naprogramoval někdo jiný (nejspíš pánové Kernighan a Ritchie, možná někdo jiný, nevím). 

Jazyk *C* pracuje na nižší úrovni, než jazyk *PHP* a nebo i *Nim*. Interpet *PHP*, nebo i kompilátor jazyka *Nim* za nás ve skutečnosti udělají spoustu práce, kterou v jazyce *C* musíš udělat sám. Ale zase je program napsaný v jazyce *C* většinou rychlejší. Také v něm můžeš psát programy, které komunikují přímo s hardware počítače -to jsou například ovladače, komunikační protokoly apod.

V jazyce *C* musím mít funkci *main*, kterou vlastně začíná program. To je vše mezi  *{* chlupatými závorkami *}*. A, že se ta funkce jmenuje *main* a ne třeba *pozdrav*, to je napsáno předtím. Prázdné *(* kulaté závorky *)* říkají, že ta funkce nevyžaduje žádné parametry. I když správnější by bylo napsat `(int argc, char *argv[])`, čímž programu řekneme kolik a jaké argumenty jsme při spuštění zadaly z příkazového řádku.

Před názvem funkce *main* je klíčové slovíčko *int*. Nejspíš by správnější bylo napsat místo něj *void* a nebo do těla funkce dopsat *return 0;*, nicméně kompilátor tomu rozumí i takto. Aspoň ten můj kompilátor. Ano, jazyk *C* je jazyk kompilovaný.

Kompilátorů jazyka "C" je celá řada, na linuxu se pro kompilaci nejčastěji používá program *gcc*, případně *cc*.

```bash
gcc src/main.c -o src/main-c
./src/main-c

```

Opět počítač pozdravil pozdravem "hello world".

Jazyk *C* se může zdát složitý. Aspoň z toho co jsem před chvíli říkal. Není to tak těžké, jak se zdá. Můžeme jazyk *C* považovat za tokový základ, který je dobré aspoň trochu znát. Většina ostatních jazyků, o kterých mluvím na jazyce *C* nějakým způsobem staví. 

### vztah mezi jazykem C, Pythonem a PHP

Jazyk *PHP* i jazyk *Python" jsou napsány v jazyce *C*. Interpet *PHP*, nebo *Pythonu*  je tedy program napsaný v jazyce *C*, který umožňuje spouštět vlastní programy - skripty. Ty se píší třeba v jazyce *PHP* - ten je sice hodně podobný jazyku *C* (na který byly programátoři už zvyklí), ale je hodně zjednodušený. *Python* má také svůj vlastní jazyk. Ten se naopak od standardu jazyka *C* docela dost liší. Nepoužívá chlupaté závorky *{* a *}* a středníky ";", místo toho je pro něj důležité odsazení nového řádku. V jazyce *C* a *PHP* můžete řádky odsazovat jak chcete, nemají syntaktický význam. Jazyk "Nim" používá syntax podobnou jazyku "Python".

Jazyk *Nim* je zajimavý ještě něčím. Kompilace funguje tak, že jeho zdrojový kód se převede do jazyka *C* a interně se pak zkompiluje jako kdyby byl program napsaný v jazyce *C*. Teoreticky by program v jazyce *Nim* mohl být stejně rychlý, jako program v jazyce *C*. A taky by měl umět stejné věci, protože si na úrovni zdrojových kódů mohu naimportovat knihovny z jazyka *C*. Prakticky to s tou rychlostí není tak úplně pravda, protože záleží na tom, jak se to do zdrojového kódu jazyka *C* automaticky převádí a přímo v *C* to lze většinou napsat o něco efektivněji.

I v jazyce *PHP* lze použít zdrojový kód z jazyka *C*. Dělá se to zpavidla pomocí *extension* a v principu to funguje tak, že se kolem nějakého kódu v jazyce *C*, jehož funkčnost chci dostat do *PHP* napíše speciální kód, je to vlastně taková obálka. Celé se to pak zkompiluje do knihovny a tu si *PHP* umí načíst, když mu jí dáme do správného adresáře. Spousta takovýchto *extension* je standardní součástí *PHP*, napsat si vlastní *extension* lze, ale v praxi se to moc často nedělá a není to úplně triviální.

V *Pythonu* je spolupráce s jazykem *C* o dost snažší. V *Pythonu* je dokonce možné hackovat zkompilované programy a třeba hledat, jak obejít licenční omezení. To ale nedělej, není to legální. Jestvuje několik řešení, jak zdrojový kód jazyka *Python* převést na jazyk *C* a zkompilovat. Tady tím se výrazně inspiroval jazyk *Nim*, který to má jako základní chování. Taktéž lze do jazyka *Python* poměrně snadno importovat knihovny napsané v jazyce *C* a jde to mnohem lépe, než v *PHP*.

> Všimni si, že když budeš chtít program napsaný v interpretovaném jazyce, třeba v *PHP*, či "Python" spustit v jiném počítači, bude muset na tomto počítači být naistalovaný interpet jazyka. Někdy dokonce záleží i na jeho verzi (zejména Python 2.x a Python 3.x nejsou kompatibilní a to ani zpětně, ale i hlavní verze PHP se vzájemně výrazně liší). V případě kompilovaného programu stačí přenést výsledný binární soubor a kompilátor na tom druhém počítačí být vůbec nemusí. Vše by mělo fungovat. Mělo. Někdy se to nepovede, i kompilovaný program může být závislý na různých sdílených knihovnách a je samozřejmě důležité, jaký je na tom druhém počítači operační systém. Program kompilovaný pro linux na windows jen tak nepoběží.

### Java, JVM, Kotlin, Scala

Přínos jazyka *C* v době jeho vzniku bylo to, že jeden zdrojový kód bylo možné zkompilovat na jakémkoliv počítači, pro který existoval kompilátor. Jednoduchý program přeložíte na Linuxu, na Apple, i na Windows. Nebo i na Rasbery pí, když na věc přijde a s trochou hackerského umu i na mobilu. Rozsáhlejší programy v praxi používají knihovny operačního systému a tak je nelze jen tak kompilovat pod jiným operačním systémem. I když na tom konkrétním hardware, s tím a tím procesorem, uspořádáním paměti atd. by samozřejmě fungovaly. Řešením tohoto problému jsou virtuální stroje. A nejznámějším jazykem, který tento způsob využívá je jazyk *Java*. Virtuální stroj je něco mezi kompilátorem a interpretem. Zdrojový kód se přeloží, nikoliv však do spustitelného binárního souboru, ale do tkzv. bytekódu. Na počítači si nainstalujete interpet jazyka "Java" - ten však nezpracovává zdrojové kódy skriptu, ale předkompilovaný bytekód. Je to jednak bezpčnější - chyby se odhalily při kompilaci - a jednak rychlejší (i když to někdy nejde úplně poznat).

Zdrojový kód programu "hello world" v jazyce Java vypadá takto.


```java
# src/main.java
<?php require __HELLO_WORLD__ . "/src/main.java"; ?>
```

Tak a to je docela jiné kafe .-). Zásadní rozdíl oproti předchozím kódům je v tom, že toto je čistý objektový kód. *Java* si zakládá na tom, že je to objektově orientovaný jazyk. Objektově orientované programování bylo hitem v 90. letech a myslelo se, že je to všespasitelný způsob programování. Vývojáři *PHP* na to skočili (nikoliv původní autor *PHP* [Rasmus Lerdorf](https://phpthewrongway.com/)), a i v *PHP* prosadili používání objektů.

V Javě musí být všechno třída, *class*. Třída má metody a metoda *main* funguje podobně jako funkce *main* v jazyce *C* - je to místo, kde vlastně začíná program.

Podobný objektový kód můžete napsat i jazyce *PHP*. Ten může být téměř identický jako tento javovský. Objektový kód v jazyce *Python* bude mírně odlišný, v základních principech podobný. *Python* je na jednu stranu hodně pružný, dá se tam s objektovým programování hodně vyblbnout. Některé věci však řeší odlišně.

Jazyk *C* objektový není, jestvuje však objektový jazyk *C++*. Ten se v zásadě dosti podobá *Javě*, není však tak striktní. Na platformě #net pak Micorsft vyvíjí jazyk #C, který se snaží brát si to nejlepší z obou.

Jazyk *Nim* má podporu objektového programování, myslím, že rozumnou a dostačující.

Postupně se ukázalo, že ta objektová cesta není až tak úžasná. Pro Javu existuje spoustu knihoven, je to jeden z nejpoužívanějších jazyků. Aby bylo možné využívat už hotové knihovny pro *Javu* a přitom se vyhnout nutnosti psát vše *objektově*, vzniklo několik jazyků, které se také překládají do baytekódu Javy, umožňují používat knihovny z jazyka "Java", ale píší se jednoduším způsobem. Jedním z nich je jazyk *Scala*, jiným třeba jazyk *Kotlin*.

```kotlin
# src/main.kt
<?php require __HELLO_WORLD__ . "/src/main.kt"; ?>
```

### Haskell, funkcionální programování

Jestvuje celá řada jazyků, které staví na úplně jiném paradigmatu, než je objektově orientované programování. Jako určitý protipól *OOP* je *funkcionální programování*. Akademickým a typickým zástupcem takového jazyka je jazyk *Haskell*. 

```haskell
# src/main.hs
<?php require __HELLO_WORLD__ . "/src/main.hs"; ?>
```


```bash
ghc -o src/main-hs src/main.hs
./src/main-c
```

Osobně jsem se pro funkcionální jazyky nadchnul, pak jsem ale přišel na to, že je dobré si z nich vzít jen něco. Poodbně je to s *OOP*. Je dobré mít možnost vytvářet objekty, ale není dobré být jejich otrokem. Je dobré mít možnost používat funkce vyššího řádu, ale není dobré být otrokem pure funkcí.

### javascript a nodejs

Na závěr ještě zmíním jeden důležitý jazyk. Je poněkud zvláštní, přitom velice důležitý. Jmenuje se *javascript*. Jeho zvláštnost spočívá v tom, že to je dnes vlastně jediný jazyk, který běží ve webovém prohlížeči. A to je důležité, protože žijeme v době webových aplikací.

Samotný *javascript* není považován za příliš povedený jazyk. Postupně se upravoval, měnila jeho specifikace. Má poněkud neobvyklý přístup k objektovému programování, používá prototypy. Důležitý pojem, se kterým se v javsacriptu setkáte je asynchroní programování a s tím souvisí pojmy jako *callback*, *callback hell* a *promises*.

Interpet jazyka javascript byl uvnitř webových prohlížečů. Dlouhou dobu se řešil problém, že každý prohlížeč používal trochu jinou verzi, zejména Microsoft do svého Internet Exploreru implementoval jinou funkcionalitu a jinou syntax, než vývojáři ostatních prohlížečů.

Nyní už je tento problém snad pomalu zažehnán a všichni výrobci tak nějak spějí k jednotnému standardu.

Program v Javascriptu tedy typicky běžel tak, že se jeho kód vložil do html stránky, kterou si uživatel otevřel v prohlížeči. A v jeho prohlížeči se vykonal, interpretoval kód toho skriptu. Program v javascriptu uměl pracovat s html dokumentem, repsetive s jeho DOM reprezentací a s oknem prohlížeče. Neuměl pracovat s vlastním počítačem, volat funkce operačního systému, zapisovat na disk a podobně.

To platí dodnes.

Zároveň byl vytvořen interpret jazyka javascript, který spouští javascriptový kód v počítači podobně jako třeba interpret Pythonu spouští kód v Pythonu. Interpet javascriptu se jmenuje *Nodejs*.

Teoreticky lze tak jeden javascriptový kód spustit jak v prohlížeči, tak aj na počítači pomocí nodejs. Jsou však jistá omezení. V počítači nemůže mít nodejs přístup k žádnému DOMu, ani k objektu window, neboť ten neexistuje. Může však být emulován. V prohlížeči zase nemůže skript přistupovat ke zdrojům počítače.

Program v javascriptu může vypadat takto.

```javascript
# src/main.hs
<?php require __HELLO_WORLD__ . "/src/main.js"; ?>
```


```bash
nodejs src/main.js

```

Stejný kód můžeme vložit do html stránky a když tuto stránku otevřeme ve webovém prohlížeči (musíme mít nějaký http server), vypíše se "hello world". Nikoliv však do stránky samé, ale do ladící konzole.

![screenshot_hello_world_main.png](../images/screenshot_hello_world_main.png)

Tenhle kód vypíše zprávu do stránky. Nelze jej však spustit v nodejs.

```javascript
# src/main.hs
<?php require __HELLO_WORLD__ . "/src/client.js"; ?>
```

![screenshot_hello_world_client.png](../images/screenshot_hello_world_client.png)

```bash

$ nodejs src/client.js 
/home/dev/pruga-book/src/client.js:1
document.write("hello world");
^

ReferenceError: document is not defined
    at Object.<anonymous> (/home/dev/pruga-book/src/client.js:1:1)
    at Module._compile (internal/modules/cjs/loader.js:778:30)
    at Object.Module._extensions..js (internal/modules/cjs/loader.js:789:10)
    at Module.load (internal/modules/cjs/loader.js:653:32)
    at tryModuleLoad (internal/modules/cjs/loader.js:593:12)
    at Function.Module._load (internal/modules/cjs/loader.js:585:3)
    at Function.Module.runMain (internal/modules/cjs/loader.js:831:12)
    at startup (internal/bootstrap/node.js:283:19)
    at bootstrapNodeJSCore (internal/bootstrap/node.js:622:3)
```

V nodejs můžeme vytvořit a spustit vlastní http server.

```javascript
# src/main.hs
<?php require __HELLO_WORLD__ . "/src/server.js"; ?>
```

```bash
$ nodejs src/server.js
Server running at http://localhost:8000/
```

a výsledek si prohlédnout ve webovém prohlížeči.

![screenshot_hello_world.png](../images/screenshot_hello_world.png)

### ECMAScript a jazyky transpilované do javascriptu

*ECMAScript* je norma pro (nejen) *Javascript* vydaná neziskovou organizací *ECMA International*. *Javascript* je jendou z implementací normy *ECMAScript*, není jedinou, ale je zdaleka nejrozšířenější.

Jazyk *Javascript* neměl u mnoha programátorů dobrou pověst. Zaslouženě. Vlastně sám javascript se začal vylepšovat a vyvíjet tak, že jeho nejnovější verze nefungovaly ve stávajících prohlížečích. Měli jsme jazyk *ECMAScript 5* a *ECMAScript 6*. Ten, aby se to lépe pamatovalo byl přejmenován na *ECMAScript 2015* a po něm přišly *ECMAScript 2016*, *ECMAScript 2017*, *ECMAScript 2018*, *ECMAScript 2019*, *ECMAScript 2020*, ... A pro nejnovější verzi se používá pohyblivý název *ES.Next*.

Vývoj prohlížečů je pomalejší, než vývoj Javascriptu. A i kdyby nebyl, všichni uživatelé nebudou nikdy používat nejnovější verze prohlížečů. Tento problém se řeší tak, že se novejší verze javascriptu transpilují do verzí starších a ty už fungují i ve starších prohlížečích.

Zdrojový kód v jednom jazyce se převede do zdrojového kódu v jiném jazyce. Tomu se říká transpilace. Je to něco podobného jako kompilátor. Rozdíl je v tom, že výstupem kompilátoru je nějaký binární kód srozumitelný tak nanejvýš procesoru. Výstupem transpilátoru je opět zdrojový kód, který je více, nebo méně srozumitelný člověku. A tento výsledný kód pak musí zpracovat buď kompilátor, nebo interpret jazyka.

Pamatuji časy, kdy se generování zdrojových kódů z jiných zdrojových kódů považovalo za něco špatného, nečistého. Časy se mění a dnes je to běžný postup.

Programátory napadlo, že když se běžně převádí třeba *ECMAScript 6* do staršího Javascriptu, že by se stejně mohl převádět i kód z jiného jazyka. A tak učinili.

> Už jsem se dříve zmínil, že jazyk *Nim* pracuje tak, že se jeho zdrojový kód nejprve transpiluje do zdrojového kódu jazyka *C* a pak zkompiluje kompilátorem jakzyka *C*. Kromě toho jazyk *Nim* umožňuje transpilaci do *Javascriptu*

Jazyk *Nim* je staticky typovaný, při transpilaci se kontrolují datové typy a výsledkem tak je bezpečný *Javascript*. Je možné do zdrojových kódů importovat javascriptové knihovny. Jejich volání je také kontrolováno, ošetřeno těmi datovými typy. Chyby se projeví při kompilaci. Nemáme však kontrolu nad tím, co se děje uvnitř těch naimportovaných knihoven.

Funkcionální přístup a důslednou kontrolu datových typů nabízí jazyk *Elm*. Jeho syntaxe je velice blízká jazyku *Haskell*, jsou tam však drobné odlišnosti. S importem cizího javascriptu se Elm vypořádává podobně jako *Nim* (ten se dost možná v *Elmu* inspiroval, nevím). Javascript běží odděleně, nemůžeme garantovat co se v něm děje, ale vstup a výstup do toho cizího kódu má Elm plně pod kontrolou a už při kompilaci hlásí chyby.

Statické typování nad Javascriptem nabízí jazyk *Typescript* od Microsoftu. Tam je však statické typování nepovinné. Lze volně míchat kód v typescriptu a javascriptu.

V minulosti byl populární jazyk *Coffescript*. Vylepšoval syntax javascriptu. Lze se s ním setkat v editoru Atom, který ho hojně využíval a část zdrojových kódů tohoto editoru a některých pluginů je stále v Coffescriptu. Samotný Coffescript už nemá smysl používat, většinu vylepšení, která přinesl jsou implementovaná v moderním javascriptu, zejména od verze ECMAscript 6 a případně v Typescriptu. Když jsem zmínil editr Atom, pak ještě dodám, že editor vscode používá pro svá rozšíření právě Typescript.

ClojureScript je další funkcionální jazyk, který se transpiluje do Javascriptu. Narozdíl od Elmu, který vychází z Haskellu, tento je založený na Lispu.

Do Javascriptu se umí transpilovat i jazyky, které se primárně kompilují do JVM. Umí to výše zmíněná *Scala* i *Kotlin*. Myslím, že i ze samotné Javy lze nějak genrovat Javascript, ale podrobnosti neznám.

Facebook představil jazyk *Reason*. Jde o další funkcionální jazyk, vychází z jazyka OCaml.

Pro Python jestvuje několik balíčků, které různým způsobem řeší genrování javascriptu z Pythonu. Zkoušel jsem několik z nich, ale nebudu se o nich raději dál zmiňovat. Nevidím tam přidanou hodnotu, kterou by to mělo přinést. Zatímco *Nim* tam dodává typovou bezpečnost, beztypový Python vlastně nemá nabídnout nic víc, než lepší syntax, tu svou, která ale nemusí vyhovovat každému.

*Emscripten* je zajimavý projekt, byť jeho praktické využití a rozšíření jsem zatím nezaznamenal. Umožňuje kompilovat (spíš kompilovat, než transpilovat) do minimalistické verze javascriptu zdrojové kódy jazyka C a C++. A protože jako mezikrok při kompilaci využívá [llvm - Low Level Virtual Machine](https://cs.wikipedia.org/wiki/LLVM), lze tímto způsobem kompilovat do javascriptu zdrojové kódy celé řady dalších jazyků - Rust, Swift, Haskell, D, Java, ...

*Dart* je jazyk od Google, který dost možná nyní zažívá renesanci jako programovací jazyk pro mobilní aplikace. Vznikl již v roce 2011 a původní záměr snad byl, že by se spouštěl přímo v prohlížeči, podobně jako javascript. To se nikdy nepovedlo. Od počátku však uměl vygenerovat i javascriptový kód pro ty prohlížeče, které neměly přímou podporu Dartu (což byly vlastně všechny, včetně Chrome od samotného Google). Dart nyní využívá platforma Flutter pro vývoj mobilních aplikací.


### shrnutí představených jazyků

**Nim** je moderní, kompilovaný, staticky typovaný jazyk. Má syntaxi podobnou Pythonu, tedy poměrně přehledný kód. Místo změti různých závorek se používá odsazování jako součást syntaxe jazyka. Výstupem je buď spustitelný program (kompilovnaý skrze kompilátor jazyka C) a nebo javascritový kód. Nim je lehký a snadno rozšiřitelný jazyk. Pomocí maker a šablon lze velice šikovně přizpůsobovat zdrojový kód.

**Python** - patří k nejpoužívanějším jazykům vůbec. Jeho kód je přehledný a vývoj v něm je velice rychlý. Dobře se propojuje s jazykem C/C++ (když potřebujeme využít nějaké knihovny, nebo naopak část vlastního kódu psaného v Pythonu kvůli rychlosti zkompilovat). Python je poměrně flexibilní jazyk. Jestvuje pro něj mnoho a mnoho balíčků na nejrůznější oblasti. Ačkoliv se hojně využívá i na webové aplikace, myslím si, že zrovna v této oblasti to není úplně nejlepší volba. I když jestvují výborné webové frameworky, robustní Django, lehký Flask, minimalistické Bottle a další, spolupráce s clientským javascriptem je komplikovaná. Jako čistě serverové řešení ve spolupráci s javascriptovým frameworkem to nicméně použít lze a používá se.

**PHP** - jazyk, na který se někteří programátoři dívají trochu skrz prsty. Nezaslouží si to. PHP stále vládne webu, běží na něm nejpoužívanější webové aplikace jako Wordpress, Joomla a Drupal. Běží na něm Wikipedie. Na PHP vznikl Facebook, ten později kvůli výkonu vyvinul [HipHop](https://cs.wikipedia.org/wiki/HipHop_for_PHP) - transpilátor zdrojových kódů PHP do optimalizovaného C++. PHP dosáhlo takové obliby díky snadnému hostování serverů, i díky relativní jednoduchosti jazyka. I když takový Python není o nic moc složitější, myslím, že největší výhodou PHP je to, že jej lze psát rovnou do zdrojových kódů html stránky. I když se od této praxe snaží většina vývojářů odrazovat, doporučují některý ze spousty různých frameworků, tlačí na používání objektového programování, sám původní autor jazyka Rasmus Lerdorf si myslí něco jiného [špatnou cestou](https://phpthewrongway.com/). Mě naopak v PHP chybí větší podpora funkcionálního stylu, autoloading funkcí (funguje jen u tříd a to je jeden z důvodů zbytečného přeceňování OOP v PHP).

**C** - to je nezbytný základ. Je moc dobré ho umět.

**Java, Scala, Kotlin, ...** - platforma JVM. Svět Javy je velký, programátoři v Javě jsou žádaní. Většinou se v javě vyvíjí rozsáhlé aplikace ve velkých korporacích. Jedna věc je umět programovat - Java není složitý jazyk - druhá věc je znát ten ekosystém kolem toho jazyka, frameworky, vývojová prostředí, způsob práce s projekty.

**javascript a nodejs** - nezbytná znalost pro moderní webové aplikace. Vývojáři se dále dělí podle toho, jaký framework ovládají - React, Vue.js, nebo jen starý dobrý javascript a jquery (stále se leckde používá). Základní znalost javascriptu (a node.js asi taky) je dobrá až nutná i tehdy, když používáš nějaký jazyk, který se do javascriptu jen kompiluje (Elm, Nim apod.).


### hafo dalších jazyků

Jestvuje spousta dalších jazyků, které jsme nezmínil.

Vynechal jsem kompletně jazyky z platformy .net (#C, #F, atd). Jsem Linuxák. Nemám ideologické předsudky vůči Micorsoftu - díky za vscode, je skvělé, díky za přehodnocení vztahu k opensource, ale díky třeba i za office, které jsem dlouho používal (na Windows). Své první programy jsem psal ve Visual Basic for Applications pro Excel. Nicméně platformu .net nesleduji. Trochu jsem se zajímal o jazyk #F, ale v pdostatě o světě .net nic nevím.

Nezmínil jsem další dva jazyky, které jsou hodně používané, mají myslím velký potenciál a zkoušel jsem v nich i něco naprogramovat.

První z nich je **Rust*. Tento jazyk možná jednou nahradí C, případně C++. Klade důraz na práci s pamětí, která je achylovou patou céčkových jazyků. Pracuje se v něm docela dobře. Jazyk je stále ve vývoji. Narazil jsem na problém, který jsem nedokázal pochopit a pak jsem objevil *Nim*. 

Druhým je jazyk **Go**. To je takový praktický jazyk, ve kterém vznikají docela dobré (nejen) webové aplikace. Dobrá náhrada za node.js.

Je vidět, že tyto dva jazyky se dost liší. Rust míří někam do nizkoúrovnějšího programování, kde vládne C, Go naopak jde spíš k vývoji moderních webových aplikací. Na Linuxdays měl Pavel Tišnovský přednášku, ve které tyto dva jazyky porovnává[Go vs. Rust](https://www.youtube.com/watch?v=IKorsr9tOFY).

Nezmínil jsem jazyk *Erlang*, kterým jsem se krátce zabýval. 

Nezmínil jsem *ActionScript*, implementaci ECMAscriptu od Adobe. Ten jazyk je už nejspíš mrtvý. Byly v něm však věci, které javascript dodnes úplně neumí. Nevýhoda byla, že se kompiloval do flashe, pluginu, který se musel instalovat do prohlížeče a byl děravý jak bota. Samotný jazyk byl ale dost dobře propracovaný a oblíbil jsem si ho.