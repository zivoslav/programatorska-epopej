## operační systém

Používám operační systém Linux, na notebooku mám desktopový [Linux Mint](https://www.linuxmint.com/), na serverech preferuji [Debian](https://www.debian.org/).

Linux je velice oblíbený na serverech. Na stolních počitačích a zejména noteboocích výrazně méně.

> Když jsem fasoval služební notebook, v několika firmách se mě hned zeptali: "Asi si tam dáš nějaký Linux jako všichni, že?". V některých velkých korporacích to pravda bylo jinak. Tam jsem dostal notebook s nainstalovaným Windows a kvůli striktní firemní bezpečnostní politice nebylo možné to nijak změnit. S každým požadavkem na instalaci nějakého software jsem musel vyplňovat formulář a žádat o pomoc pana administrátora. Ve většině firem si ale na svém (i služebním) počítači můžete dělat co chcete. Bezpečnost se řeší na úrovni přístupu ke konkrétním projektům, do jejich repozitářů, na konkrétní servery, do privátních sítí atd.

A pak jsou tu ještě spokojení uživatelé počítačů od Apple.

Vše o čem v Programátorské epopeji píšu by mělo fungovat ve všech distribucích Linuxu, na Windows i na počítačích Apple. Ale postup se může mírně lišit. Na internetu jistě vždy najdeš řešení pro svůj operační systém.

> Příkaz *apt* použitý v minulé kapitole je zrovna příkladem příkazu, který funguje pouze na *Linuxu* a to ještě jen na distribucích založených na *Debianu*. Zrovna správa softwarových balíčků je oblast, kde se jednotlivé linuxové distribuce dost liší. Jinak ale většina věcí funguje téměř stejně na všech Linuxech a často i na Apple a Windows.

Linux Mint a Debian se od sebe v zásadě příliš neliší. Distribuce Linux Mint je založena na populární distribuci Ubuntu a ta je zase založena na Debianu. A jsou vzájemně komapatibilní. Zkoušel jsem různé linuxové distribuce, chceš-li také experimentovat, podívej se na [distrowatch.com](https://distrowatch.com/). Najdeš tam srovnání a žebříčky různých distribucí. Nakonec jsem zůstal u distribuce Linux Mint ze tří důvodů:
- je založena na Ubuntu, které je velice rozšířené a většina návodů a rad na internetu pro Ubuntu funguje i v Mintu.
- Mint se vlastně od Ubuntu liší jen v tom, že se snaží mít dokonale vyladěné grafické uživatelské rozhraní. Dřív mě bavilo si měnit nastavení plochy, lišt a tak dále, ale už mě to přešlo. A v Mintu je to udělané dobře, vypadá to hezky a funguje to.
- v Mintu funguje úplně vše jako na serveru s Debianem, takže si věci, které vyvýjím pro server můžu zkoušet přímo na svém pracovním počítači.

Na serveru používám Debian, protože to je na serverech jedna z nejpoužívanějších distribucí. Tak jako Mint klade důraz na uživatelskou přívětivost, Debian klade důraz na to, aby dobře běžel na serverech.

Nikomu nevnucuji ani Linux Mint ani Debian. Budu jen rád, když vyzkoušíš něco jiného. Používej si, co se ti líbí. Je to tvá volba. Zvolíš-li si jiné řešení, než já, budeš si muset občas dohledat některé postupy a informace. A to vůbec není na škodu.
