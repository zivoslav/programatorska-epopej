## správa verzí

Dobrá rada na začátek. Používejte nějaký systém pro správu verzí. Nenapadá mě žádný důvod, proč používat něco jiného, než [Git](https://cs.wikipedia.org/wiki/Git). Jasně, najdete lidi, kteří na něj nadávají. I mě několikrát přivedl do stavu zoufalství. Nic není dokonalé. Důvody proč používat právě *Git* jsou přímočaré. Je to nejpoužívanější systém, jsou v něm verzovány významné projekty, včetně třeba jádra Linuxu. Nejspíš se tento systém bude dál úspěšně vyvíjet, zlepšovat a určitě právě k němu najdeš na internetu pomoc, či radu, když budeš v nesnázích.

*Git* je program, který se ovládá z příkazového řádku. [Příkazový řádek](https://cs.wikipedia.org/wiki/P%C5%99%C3%ADkazov%C3%BD_%C5%99%C3%A1dek) (**CLI** - *Command Line Interface*) se spouští v *terminálu*. *Terminál* je jednoduchý program, který vypíše [prompt](https://cs.wikipedia.org/wiki/P%C5%99%C3%ADkazov%C3%BD_%C5%99%C3%A1dek#Prompt) a čeká na příkazy, které napíšeš.

- je to základní rozhraní ovládání počítače
- spoustu programů a zejména vývojářských nástrojů lze spustit příkazem v terminálu (tedy v příkazovém řádku, dost často se používá zkratka *cli* i v názvu programu, například *webpack-cli*, nebo "babel/cli") a je to většinou nejrychlejší a nejefektivnější způsob práce
- když se vzdáleně připojíš na server, nebo třeba na svůj mobil přes *Android sdk*, bez znalosti příkazového řádku se neobejdeš
- příkazy pro příkazový řádek lze zapsat do souboru, vytvořit tak vlastně jednoduchý program, zvaný skript a ten spouštět kdykoliv podle potřeby. Hodně věcí se tím usnadní.

Tady je kompletní výpis z mého terminálu. Chtěl jsem spustit program *git*, ale nejdříve jsem ho musel nainstalovat. Celý tenhle výpis můžeš přeskočit. Je to jen demonstrace práce v terminálu. Jsou tam použité pouze dva příkazy *git*, který selhal, neboť nebyl nainstalován a pak příkaz "apt install git", respektive "sudo apt install git" pro instalaci *Gitu*. Většina řádků obsahuje výpis programu během instalace, co se děje, co se instaluje a tak dále. Většinou, respektive, když vše funguje jak má, tak tyto informace nejsou k ničemu potřeba. Pokud něco selže, lze zjistit co selhalo a zkusit chybu napravit.

Příkaz "apt install git" selhal, protože pro instalaci softwarových balíčků nemám orávnění. Pomocí příkazu *sudo* jsem instalaci spustil znova jako superuživatel zvaný *root*. Ne každý si může spouštět příkazy jako *root*, ale já naštěstí na svém počítači tu možnost mám. Kdo není pánem svého počítače, musí vyhledat pana administrátora a poprosit ho o pomoc.

<?php require __DIR__ . "/../examples/git-install.md" ?>

Dřív, než pokročíme dál, zastavme se ještě u jednoho ožehavého tématu.
