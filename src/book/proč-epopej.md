## Proč epopej?

Když jsem byl ještě mladý kluk a zkoušel psát své první programy, zaujal mne příběh jednoho slavného vědce a programátora. Jmenuje se [Donald Ervin Knuth](https://en.wikipedia.org/wiki/Donald_Knuth).

Donald Ervin Knuth je známý především jako autor mnohasvazkové odborné monografie [Umění programování (The Art of Computer Programming)](https://cs.wikipedia.org/wiki/Um%C4%9Bn%C3%AD_programov%C3%A1n%C3%AD) a jako autor typografického programu [TeX](https://cs.wikipedia.org/wiki/TeX). A také se proslavil ještě dalším pozoruhodným počinem. A ten počin se jmenuje **literární programování**.

Jedná se vlastně o programovací paradigma, ve kterém je počítačový program a jeho logické řešení vysvětleno v přirozeném jazyce, jakým je například angličtina. V mém případě tedy čeština. Zdrojový kód a dokumentace spolu tvoří jakousi *esej*, ze které speciální program vytáhne a přeloží zdrojové kódy programu. Zároveň lze celou *esej*, včetně vložených zdrojových kódů prezentovat různými způsoby - například jí vysázet jako knihu, či článek, nebo z ní vytvořit webovou stránku.

> slovo *literární* pochází z latinského *littera*, písmeno

> slovo *epopej* pochází z řeckého *εποσ (epos)* = slovo, pověst a *ποειο (poeio)* = dávat dohromady, skládat

Zvolil jsem si řecký výraz *epopej* pro své vyprávění o programování. Do svého příběhu budu vkládat zdrojové kódy, ze kterých bude možné zkompilovat funkční kód. Tento literární žánr se jmenuje **literární programování**.

Programátorská epopej vypráví o tom, jak softwarový projekt vzniká. Přiznávám, že už ten software mám vytvořený, ovšem při psaní této epopeje ho vytvoříme společně znovu. Znovu a lépe. Už vím jak a co a čemu se vyhnout a co bylo dobře a tak. Budu ti průvodcem, ale nebudu ti vysvětlovat žádné složité teorie, spíše jen principy, ukážu jak věci fungují a hlavně budeme řešit reálné problémy. Projdeš-li touto epopejí, naučíš se rozpoznávat zrádná zákoutí softwarového vývoje. Chceš-li je jen používat hotový program, který v programátorské epopeji vytvoříme, pokračuj @TODO: link to final version  
