## editor

Už bychom mohli začít programovat. Potřebujeme k tomu ještě dobrý editor.

Opravdoví hackeři používají [Vim](https://cs.wikipedia.org/wiki/Vim). Také bych s ním chtěl někdy umět.

Kromě editorů jestvují ucelená vývojová prostředí [IDE](https://cs.wikipedia.org/wiki/V%C3%BDvojov%C3%A9_prost%C5%99ed%C3%AD). Jsou většinou zaměřena na jeden jazyk a kromě editorů nabízí další nástroje. Používal jsem Phpstorm a zend studio pro PHP, Eclipse (a jeho klony) pro Python a C/C++. Pro Python i méně známé IDE Eric, to se mi celkem líbilo.

Nic proti IDE, ale myslím, že nejsou potřeba. Moderní editory je umí plně nahradit.

Moderními editory myslím především tyto tři
- [Sublime text](https://www.sublimetext.com/)
- [Atom](https://atom.io/)
- [Visual studio code (vscode, code)](https://code.visualstudio.com/)


- **Sublime text** ukázal cestu. Není zdarma a není open source a znám řadu vývojářů, kteří si licenci rádi zaplatili. Rozšíření do něj se píšou v Pythonu.
- **Atom** je v principu Sublime textu podobný. Je psaný v nodejs, javascriptu, postavený na electronu (to je prostředí, které z webových aplikací dělá aplikace desktopové). Je to open source, jeho vývoj podporuje společnost github.
- **vscode** - je hodně podobný Atomu. Hodně vývojářů raději používalo Atom, protože se jim zdál svobodnější, než vscode, ačkoliv i vscode je open source. Důvodem byla nejspíš přetrvávající nedůvěra k Microsoftu, který byl dříve znám jako nepřítel open source a tvrdý prosazovač proprietárních řešení (hlavně svých - Windows, MS Office, ...). Jenže github, který stojí za Atom editorem koupil Microsoft, takže je to už celkem jedno.

Já jsem přešel na *vscode*, protože *Atom* byl jeden čas dost pomalý. To už asi není úplně pravda, zapracovali na tom. *Atom* měl dříve větší komunitu, dnes se mi zdá, že už se to vyrovnalo. Potřebná rozšíření jsou většinou dostupná jak pro *Atom*, tak i pro *vscode*. Poslední dobou se mi jeví, že komunita kolem *vscode* je aktivnější a sám i sám Microsoft projekt aktivně podporuje. *Atom* už také patří pod Microsoft (s githubem), slíbily však, že ho nezaříznou (mají tak vlastně dva dost podobné produkty). Ale stejně myslím, že to je pomalý konec Atomu.
