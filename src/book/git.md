## Git

Tak zpět k tomu *gitu*. Máme ho naistalovaný a vytvoříme si nový repozitář. V něm vytvoříme nový soubor, a změny nahrajeme. Může to vypadat třeba takto. Použité příkazy *cd, ls, cat, more* budou tentokrát fungovat ve všech běžných linuxových distribucích. Pokud některé neznáš, zkus třeba [wiki.ubuntu.cz](https://wiki.ubuntu.cz/z%C3%A1kladn%C3%AD_p%C5%99%C3%ADkazy), tam se dozvíš nejen to jaké příkazy existují, ale i to, jak získat nápovědu k nim přímo v terminálu. Příkazy samotného *gitu* budou fungovat samozřejmě všude, kde je naistalovaný *git*.

<?php require __DIR__ . "/../examples/git-init.md" ?>

Nastuduj si ještě příkazy git pull, git push, git branch a něco o větvení projektu. Sdružení cz.nic vydalo v českém překladu knihu Scotta Chacona [PRO GIT](https://knihy.nic.cz/), která je myslím docela dobrou volbou pro pochopení problematiky distribuované správy verzí. No, pochopení. Ono je to docela náročné téma. *Git* je mocný nástroj, ale lidi jsou jen lidi. V týmu není snadné nastavit si pravidla, jakým způsobem *Git* používat, řešit dohady o tom, co kam nahrávat, řešit kolize při mergování a tak vůbec. Navíc ne každý je s *Gitem* úplný kamarád, včetně mě tedy, přiznávám.


## Github, Gitlab a Gitea

Nedoporučuji začínat nový projekt pomocí příkazu *git init* jak jsem ukázal v předchozí kapitole. Mnohem lepší je umístit projekt na nějakém serveru, odkud si ho pak stáhneš a mohou si ho stáhnout i další vyvojáři. K tomu ostatně je Git primárně určen.

Je možné na nějakém vlastním serveru nainstalovat Git a tam pomocí příkazu *git init* vytvářet nové projekty. Ze svého počítače si je pak stáhneš pomocí *git clone <url serveru>*, stáhneš změny pomocí *git pull* a nahraješ pomocí *git push*.

Jestvuje však lepší způsob.

Na serveru je lepší mít nikoliv čistý *Git*, ale webovou aplikaci, která interně s *Gitem* pracuje a navíc nabízí webové rozhraní pro práci s repozitářy.

Není nutno takový server vytvářet, je možno použít některou z jestvujících služeb. 

Miliony repozitářů najdeš na [github.com](https://github.com/). Je to největší veřejné úložiště. V roce 2018 ho koupila firma Microsoft, někteří vývojáří se toho zalekli a odešli jinam. Myslím však, že obavy, které mnozí z příchodu softwarového giganta měli se nevyplnily a že jich zase tolik neodešlo. Naopak se některé věci pro nezávislé vývojáře dokonce zlepšily. Od 7. ledna 2019 je možné ukládat bezplatně i soukromé repositáře, dříve to bylo možné jen po zaplacení měsíčního poplatku. Microsoft svou politiku k open source výrazně přehodnotil.

Jestvuje jedna povedená alternativa (konkurence) a jmenuje se [gitlab.com](https://gitlab.com/). Osobně jsme z githubu přešel na gitlab (účty mám na obou, používám nyní víc ten na gitlabu). Obě platformy jsou si dosti podobné, alespoň pro základní použití. Výhodou *gitlabu* bylo, že byl zdarma i pro soukromé repozitáře, ale to nyní nabízí už i *github*. Software, na kterém běží *gitlab* je možné si stáhnout a provozovat na vlastním serveru, myslím, že *github* toto nabízí za poplatek.

Více zodpoví strýček google například na dotaz [github vs. gitlab](https://www.google.com/search?q=github+vs+gitlab). Filtruj raději jen příspěvky za poslední rok, případně i kratší období.

Třetí zajimavou možností je projekt [gitea.io](https://gitea.io). Nyní zkouším a jsem spokojený. Má být rychlejší, než *gitlab*, je to samozřejmě také open source. Instalace je snadná, stačí stáhnout a spustit binární soubor, případně nastavit *service* v Linuxu. Webovou službu, kde je možno si založit účet podobně jako na *githubu*, či *gitlabu* najdeš na [gitea.com](https://gitea.com).

> Založ si účet na [github.com](https://github.com/) a (nebo) na [gitlab.com](https://gitlab.com/), vytvoř si tam nový projekt a stáhni si repozitář do počítače pomocí *git clone*. Případně vyzkoušej i [gitea.com](https://gitea.com).

## nový projekt

Při vytváření nového projektu ve webovém rozhraní (github, gitlab, gitea) si můžeš nechat vygenerovat několik souborů.

- **Readme.md** - stalo se dobrým zvykem do tohoto souboru napsat stručné informace o projektu, stručnou nápovědu, způsob jeho používání, zkrátka vše důležité.  Nejčastěji se píše ve formátu [Markdown](https://cs.wikipedia.org/wiki/Markdown). Výhoda je, že je ten soubor docela dobře čitelný ve zdrojové formě a zároveň se snadno převádí do jiných formátů, především teda *html*. Github, gitlab a gitea tento soubor používají jako "home page" projektu. Tedy tak němu přistupuj a piš tam to, co by mělo na hlavní stránce projektu být.

- **.gitignore** - soubor s tečkou na začátku, takže se někdy nezobrazí ve výpisu souborů, neb se považuje za skrytý (*ls* versus *ls -a*, grafický správce souborů). Jak název napovídá, jde o soubor *gitu*. Zapisují se do něj názvy souborů a adresářů, které nechceme aby se verzovaly (chceme, aby je git ignoroval - *gitignore*).

- **LICENSE** - samostatní svobodní vývojáři občas licenci neřeší, je to však poměrně důležitá věc. I když spíš pro právníky. Licencím nerozumím a když něco dávám jako open source, používám zpravidla licenci *MIT*, jda s většinou. ![nejpoužívanější open source licence](../images/PERMISSIVE-VS-COPYLEFT-LICENSES-2.jpg)
