```bash
pruga@domogled:~$ git init novy_projekt
Initialized empty Git repository in /home/pruga/novy_projekt/.git/
pruga@domogled:~$ cd novy_projekt/
pruga@domogled:~/novy_projekt$ ls -al
total 4
drwxr-xr-x 3 pruga pruga  3 Sep  3 15:58 .
drwxr-xr-x 3 pruga pruga  7 Sep  3 15:58 ..
drwxr-xr-x 7 pruga pruga 10 Sep  3 15:58 .git
pruga@domogled:~/novy_projekt$ cat > novy_soubor
Toto je nový soubor

^C
pruga@domogled:~/novy_projekt$ ls -al
total 4
drwxr-xr-x 3 pruga pruga  4 Sep  3 15:58 .
drwxr-xr-x 3 pruga pruga  8 Sep  3 16:02 ..
drwxr-xr-x 8 pruga pruga 13 Sep  3 16:02 .git
-rw-r--r-- 1 pruga pruga 22 Sep  3 15:58 novy_soubor
pruga@domogled:~/novy_projekt$ more novy_soubor 
Toto je nový soubor

pruga@domogled:~/novy_projekt$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	novy_soubor

nothing added to commit but untracked files present (use "git add" to track)
pruga@domogled:~/novy_projekt$ git add novy_soubor 
pruga@domogled:~/novy_projekt$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

	new file:   novy_soubor

pruga@domogled:~/novy_projekt$ git commit -m "přidán nový soubor"

*** Please tell me who you are.

Run

  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"

to set your account's default identity.
Omit --global to set the identity only in this repository.

fatal: unable to auto-detect email address (got 'pruga@domogled.(none)')
pruga@domogled:~/novy_projekt$
pruga@domogled:~/novy_projekt$ git config --global user.email "petr.bolf@domogled.com"
pruga@domogled:~/novy_projekt$ git config --global user.name "Petr Bolf"
pruga@domogled:~/novy_projekt$ git commit -m "přidán nový soubor"
[master (root-commit) 249279f] přidán nový soubor
 1 file changed, 2 insertions(+)
 create mode 100644 novy_soubor
pruga@domogled:~/novy_projekt$ git status
On branch master
nothing to commit, working tree clean
pruga@domogled:~/novy_projekt$
```